package com.shao.demo.canalclient;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author zhiqi.shao
 * @Date 2018/6/5 16:55
 */
@EnableScheduling
@SpringBootApplication
public class CanalClientApplication {


    public static void main(String[] args) {
        SpringApplication.run(CanalClientApplication.class, args);
    }
}
