package com.shao.demo.canalclient.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zhiqi.shao
 * @Date 2018/6/6 10:02
 */
@Component
@ConfigurationProperties(prefix = "canal")
@Data
public class AppConfig {

    private String ip;
    private int port;
    private String destination;
    private String username;
    private String passport;
}