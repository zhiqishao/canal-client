package com.shao.demo.canalclient.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.shao.demo.canalclient.event.InsertCanalEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author zhiqi.shao
 * @Date 2018/6/6 14:24
 */
@Slf4j
@Component
public class InsertCanalListener extends CanalListener<InsertCanalEvent>{

    @Override
    protected void doSync(String database, String table, CanalEntry.RowData rowData) {
        log.info("-------> database:{},table:{},eventType: INSERT",database,table);
        List<CanalEntry.Column> columns = rowData.getAfterColumnsList();
        //todo 过滤得到需要同步的数据
        Map<String, Object> dataMap = parseColumnsToMap(columns);
        log.info("-------> columns to json:{}", JSON.toJSON(dataMap));
        //todo 进行同步操作
    }
}
