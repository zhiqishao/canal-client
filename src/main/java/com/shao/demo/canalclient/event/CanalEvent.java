package com.shao.demo.canalclient.event;

import com.alibaba.otter.canal.protocol.CanalEntry;
import org.springframework.context.ApplicationEvent;



/**
 * ApplicationContext中的事件处理是通过ApplicationEvent类和ApplicationListener接口来提供的，
 * 通过ApplicationContext的publishEvent()方法发布到ApplicationListener;
 * 在这里包含三个角色：被发布的事件，事件的监听者和事件发布者。
 * 事件发布者在发布事件的时候通知事件的监听者。
 * spring的事件需要遵循以下流程：
 * （1）自定义事件：继承ApplicationEvent   当前类的作用
 * （2）定义事件监听器：实现ApplicationListener
 * （3）使用容器发布事件
 * @author zhiqi.shao
 * @Date 2018/6/6 13:53
 */
public abstract class CanalEvent extends ApplicationEvent {

    /**
     * 每个事件的资源定义
     * @param source
     */
    public CanalEvent(CanalEntry.Entry source) {
        super(source);
    }

    /**
     * 暴露获取对象事件方法
     * 第二部监听器监听到事件后，需要获取事件对象资源进行处理
     * @return
     */
    public CanalEntry.Entry getEntry(){
      return  (CanalEntry.Entry) source;
    }

}
